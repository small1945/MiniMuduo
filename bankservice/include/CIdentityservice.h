#ifndef CIDENTITYSERVICE_H_
#define CIDENTITYSERVICE_H_
#include"CBankservice.h"

class Identityservice:public Bankservice
{
    public:
        Identityservice(Mysqlconn*conn):Bankservice(conn){}
        virtual ~Identityservice();
        virtual void parseMessage(const std::string&)=0;
        virtual void excuteSQL()=0;
        virtual void onResponce()=0;
        void startservice(const std::string&);

    protected:
        std::string m_passwd;
};





#endif