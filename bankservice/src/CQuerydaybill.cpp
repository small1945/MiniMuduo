#include"CQuerydaybill.h"
#include "boost/make_shared.hpp"
#include <sstream>
Querydaybill::~Querydaybill()
{
}
void Querydaybill::parseMessage(const std::string &message)
{
    auto begin = message.find_first_of('=');
    auto end = message.find_first_of('&');
    m_date = message.substr(begin + 1, end - begin - 1);
}

void Querydaybill::excuteSQL()
{
    std::stringstream ss;
    //char command[300] = "";
    ss<<"select account_id,other_account_id,balance,money,trans_time from trans where (account_id='"<<m_account.c_str()<<
    "'or other_account_id='"<<m_account.c_str()<<"') and date_format(trans_time,'%Y-%m-%d')='"<<m_date.c_str()<<"';";
    m_recordSet = m_sqlconn->querySQL(ss.str().c_str());
}

void Querydaybill::onResponce()
{
    if (m_recordSet.isEmpty())
    {
        m_targetvalue = HTMLPATH + "queryError.html";
    }
    else
    {
        m_trans = boost::make_shared<Transmessage>();

        for (int i = 0; i < m_recordSet.rowSize(); i++)
        {
            m_trans->accountId = m_recordSet.getItem(i, "account_id");
            m_trans->otherId = m_recordSet.getItem(i, "other_account_id");
            m_trans->balance = m_recordSet.getItem(i, "balance");
            m_trans->money = m_recordSet.getItem(i, "money");
            m_trans->date = m_recordSet.getItem(i, "trans_time");

            std::stringstream ss;
            ss << "accountid:" << m_trans->accountId << " "
               << "other_account_id:" << m_trans->otherId << " "
               << "balance:" << m_trans->balance << " "
               << "money:" << m_trans->money << " date:" << m_trans->date<<std::endl;

           m_targetvalue+=ss.str();
        }
         m_resultIsFile=false;

    }
}