#include "chatclient.h"

Chatclient::Chatclient(int port, EventLoop *loop, const char *ip) : m_client(port, loop, ip)
{
    m_client.setConnectCallback(bind(&Chatclient::onConnect, this, _1));
    m_client.setMessageCallback(bind(&Chatclient::onMessage, this, _1, _2));
    m_client.setWriteCompleteCallback(bind(&Chatclient::onWriteCompelte, this, _1));
    m_client.setCloseCallback(bind(&Chatclient::onClose, this));
}
void Chatclient::onClose() {}
Chatclient::~Chatclient() {}
void Chatclient::write(const string &str)
{
    if (m_conn)
    {
        lock_guard<mutex> lock(m_mutex);
        m_coder.send(m_conn, str);
    }
}
void Chatclient::connect()
{
    m_client.start();
}
void Chatclient::disconnect()
{
    m_client.destoryConnection();
}
void Chatclient::onConnect(const boost::shared_ptr<TcpConnection> &conn)
{
    lock_guard<mutex> lock(m_mutex);
    if (conn->IsConnected())
    {
        m_conn = conn;
    }
}

void Chatclient::onMessage(const boost::shared_ptr<TcpConnection> &conn, Buffer *buf)
{
    int bytes = buf->readableBytes();
    auto str = buf->retrieveStr(bytes);
    printf("%s\n", str.c_str());
}

void Chatclient::onWriteCompelte(const boost::shared_ptr<TcpConnection> &conn)
{
    // printf("write complete\n");
}
