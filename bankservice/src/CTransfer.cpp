#include "CTransfer.h"


Transfer::Transfer(Mysqlconn *conn, std::string user): Accountservice(user, conn)
{

}

Transfer::~Transfer()
{
}

void Transfer::parseMessage(const std::string &message)
{
    auto begin = message.find_first_of('=');
    auto end = message.find_first_of('&');
    m_targetUser = message.substr(begin + 1, end - begin - 1);
    begin = message.find_last_of('=');
    m_amount = stof(message.substr(begin + 1, message.size() - begin - 1));
}

void Transfer::onResponce()
{

    char command[100] = "";
    sprintf(command, "select account_id from bank_account where account_id='%s' ", m_targetUser.c_str());
    m_recordSet = m_sqlconn->querySQL(command);
    if(m_recordSet.isEmpty())
    {
         m_targetvalue = HTMLPATH + "/transferError.html";
         return;
    }

    sprintf(command, "select balance from bank_account where account_id='%s' ", m_account.c_str());
    m_recordSet = m_sqlconn->querySQL(command);
    float balance = stof(m_recordSet.getItemByPosition(0,0));
    
    if (balance < m_amount)
    {
        m_targetvalue = HTMLPATH + "/transferError.html";
    }
    else
    {
        char command[150] = "";
       
        m_sqlconn->excuteSQL(command);
        sprintf(command, "update bank_account set balance=balance-%.2f where account_id='%s'", m_amount, m_account.c_str());
        m_sqlconn->excuteSQL(command);
        
    
        sprintf(command, "update bank_account set balance=balance+%.2f where account_id='%s'", m_amount, m_targetUser.c_str());
        m_sqlconn->excuteSQL(command);

         sprintf(command, "insert into trans (account_id,other_account_id,money,balance,trans_time) values ('%s','%s',%.2f,%.2f,NOW())",m_account.c_str(),m_targetUser.c_str(),m_amount,balance);
         m_sqlconn->excuteSQL(command);
         m_targetvalue = HTMLPATH + "/transferSuccess.html";
    }

}