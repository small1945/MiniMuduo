#include "CWebServer.h"
#include "boost/make_shared.hpp"
#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <sys/mman.h>
#include <string.h>
#include <CMysqlconn.h>
#include <CMysqlrecordset.h>
#include "CBankservice.h"
#include "CLogin.h"
#include "CRegister.h"
#include"CChangepasswd.h"
#include"CLogout.h"
#include"CDeposit.h"
#include"CTransfer.h"
#include"CWithdraw.h"
#include"CQuerybalance.h"
#include"CQuerydaybill.h"
Webserver::Webserver(int port, EventLoop *loop, const char *ip)
    : m_server(port, loop, ip),
      m_filename(),
      m_fd(-1)
      
{
    m_server.setRequestCallBack(boost::bind(&Webserver::onRequest, this, _1,_2));
}

Webserver::~Webserver()
{
    if (m_filestat.st_size != 0)
    {
        munmap(&m_filestat, m_filestat.st_size);
    }
}

Httpresponse Webserver::onRequest(const boost::shared_ptr<TcpConnection>&conn,const Httprequest &request)
{
    auto ptsql = m_sqlconnpool.getConnect();
    Mysqlconn sqlconn(ptsql);
    m_sqlconn = &sqlconn;

    Httpresponse httpresponse;
    httpresponse.setVersion(request.version());
    httpresponse.setServername("gc");
    // if(request.IsKeepAlive()){httpresponse.setConneciton("keep-alive");}
    httpresponse.setConneciton("keep-alive");
    if (!request.IsLegal())
    {
        httpresponse.setStatus(Httpresponse::Status::ERROR);
        return httpresponse;
    }

    httpresponse.setStatus(Httpresponse::Status::OK);
    auto url = request.url();

    if (request.methon() == Httprequest::Methon::GET)
    {
        _handleGet(url);
    }
    else if (request.methon() == Httprequest::Methon::POST)
    {
        _handlePost(url, request.body(),conn);
    }

    _setResponseBody(httpresponse);
    return httpresponse;
}

void Webserver::_handleGet(const string &url)
{
    if (url == "/")
    {
        m_filename = HTMLPATH+"judge.html";
    }
}

void Webserver::_handlePost(const string &url, const string &body,const boost::shared_ptr<TcpConnection>&conn)
{
    boost::shared_ptr<Bankservice> bankservice;
    if (url.size()<=3) // new user,prepare to register
    {
        if (url == "/0")
        {
            m_filename = HTMLPATH+"register.html";
        }

        else if (url == "/1")
        {
            m_filename = HTMLPATH+"log.html";
        }
        else if (url == "/2")
        {
            m_filename = HTMLPATH+"changepasswd.html";
        }
        else if (url == "/3")
        {
            m_filename = HTMLPATH+"logout.html";
        }
        else if(url=="/5")
        {
            m_filename=HTMLPATH+"deposit.html";
        }
        else if(url=="/6")
        {
             m_filename=HTMLPATH+"query.html";
        }
        else if(url=="/61")
        {
              m_filename=HTMLPATH+"querydaybill.html";
        }
        else if(url=="/7")
        {
             m_filename=HTMLPATH+"withdraw.html";
        }
        else if(url=="/8")
        {
            m_filename=HTMLPATH+"transfer.html";
        }
        else if(url=="/10")
        {
            m_filename=HTMLPATH+"welcome.html";
        }
    }
    else
    {
        string &accountID=usermap[conn];
       
        if (url == "/LOGCGISQL.cgi") // log
        {
            bankservice = boost::make_shared<Login>(m_sqlconn);
        }
        else if (url == "/REGISTERCGISQL.cgi") // register
        {
            bankservice = boost::make_shared<Register>(m_sqlconn);
        }
        else if(url=="/CHANGEPWCGISQL.cgi")
        {
            bankservice = boost::make_shared<Changepasswd>(m_sqlconn);
        }
        else if(url=="/LOGOUTCGISQL.cgi")
        {
             bankservice = boost::make_shared<Logout>(m_sqlconn);
        }
        else if(url=="/DEPOSITCGISQL.cgi")
        {
            bankservice=boost::make_shared<Deposit>(m_sqlconn,accountID);
        }
        else if(url=="/TRANSFERCGISQL.cgi")
        {
            bankservice=boost::make_shared<Transfer>(m_sqlconn,accountID);
        }
        else if(url=="/WITHDRAWCGISQL.cgi")
        {
            bankservice=boost::make_shared<Withdraw>(m_sqlconn,accountID);
        }
        else if(url=="/QUERYBALANCECGISQL.cgi")
        {
            bankservice=boost::make_shared<Querybalance>(m_sqlconn,accountID);
        }
        else if(url=="/QUERYDAYBILLCGISQL.cgi")
        {
             bankservice=boost::make_shared<Querydaybill>(m_sqlconn,accountID);
        }

        if (bankservice)
        {
            bankservice->startservice(body);

            if(bankservice->resultIsFile())
            {
                m_filename = bankservice->getTarget();
            }
            else
            {
                m_bodymessage=bankservice->getTarget();
            }

            if(usermap[conn].size()==0)
            {
                usermap[conn]=bankservice->getAccount();
            }
        }
    }
}

void Webserver::_setResponseBody(Httpresponse &httpresponse)
{
    if(m_filename.empty())
    {
        httpresponse.setBody(m_bodymessage);
        return;
    }

    
    if (stat(m_filename.c_str(), &m_filestat) < 0)
    {
        printf("m_filename:%s \n",m_filename.c_str());
        printf("error is: %s\n", strerror(errno));
        httpresponse.setStatus(Httpresponse::Status::NOTFOUND);
    }
    else
    {
        int filesize = m_filestat.st_size;
        m_fd = open(m_filename.c_str(), O_RDONLY);
        void *fileaddress = mmap(NULL, filesize, PROT_READ | PROT_EXEC, MAP_SHARED, m_fd, 0);
        m_bodymessage.reserve(filesize);
        m_bodymessage = (char *)fileaddress;
    }
    httpresponse.setBody(m_bodymessage);
    m_filename.clear();
}

void Webserver::start()
{
    m_server.start();
    m_sqlconnpool.init();
}