#ifndef CHTTPSERVER_H_
#define CHTTPSERVER_H_


#include <sys/epoll.h>
#include <stdio.h>
#include <sys/socket.h>
#include "CSocket.h"
#include <iostream>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include "CEpoll.h"
#include "Thread.h"
#include "CEventLoop.h"
#include "CAcceptor.h"
#include "CTcpServer.h"
#include "CInetaddr.h"
#include "CEventLoopthread.h"
#include "CHttpcontext.h"
#include <boost/bind.hpp>
using namespace std;

class HttpServer
{
public:
    using RequestCallback = boost::function<Httpresponse(const boost::shared_ptr<TcpConnection>&,const Httprequest &)>;
    HttpServer(int port, EventLoop *loop, const char *ip) ;
    ~HttpServer() {}
    void start();
    void setRequestCallBack(const RequestCallback &cb) { m_requestCb = cb; }
    void onMessage(const boost::shared_ptr<TcpConnection> &conn, Buffer *buf);
    void onConnect(const boost::shared_ptr<TcpConnection> &conn);
    Httpresponse defaultRequest(const Httprequest &request);
   
      
    
private:
    EventLoop *m_loop;
    Tcpserver m_server;
    Httpcontext m_httpcontext;
    Httpresponse m_response;
    RequestCallback m_requestCb;

};
















#endif