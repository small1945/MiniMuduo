#ifndef CMYSQLRECORDSET_H_
#define CMYSQLRECORDSET_H_
#include"mysql/mysql.h"
#include<vector>
#include<string>
#include"CMysqlconn.h"
class Mysqlrecordset
{
    public:
        friend class Mysqlconn;
        using FIELDS= std::vector<std::string>;
        using ROWS=std::vector<std::vector<std::string>>;
        
        Mysqlrecordset(){}
        ~Mysqlrecordset(){}
      
        const std::string& getItemByPosition(int row,int col);
        const std::string& getItem(int row,const std::string&field);

        void clear();
        int rowSize()const{return rows.size();}
        bool isEmpty()const{return rows.empty();}

    private:

        int _getFieldIndex(const std::string&field);
        int _getValueIndex(const std::string&value);
       FIELDS fields;
       ROWS rows;


};











#endif