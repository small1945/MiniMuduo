#ifndef CQUERYSERVICE_H_
#define CQUERYSERVICE_H_

#include<sstream>
#include"CBankservice.h"

class Queryservice:public Bankservice
{
    public:
        Queryservice(const std::string account,Mysqlconn*conn):Bankservice(conn,account)
        {
            
        }
        virtual ~Queryservice();
        virtual void parseMessage(const std::string&)=0;
        virtual void excuteSQL()=0;
        virtual void onResponce()=0;
        void startservice(const std::string&);
    
   
     

};






#endif