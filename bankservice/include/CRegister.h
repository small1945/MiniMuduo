#ifndef CREGISTER_H_
#define CREGISTER_H_
#include "CIdentityservice.h"

class Register : public Identityservice
{
public:
    Register(Mysqlconn*conn);
    virtual ~Register();
    virtual void parseMessage(const std::string &message) override;
    virtual void excuteSQL() override;
    virtual void onResponce() override;
};

#endif