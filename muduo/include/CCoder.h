#ifndef CCODER_H_
#define CCODER_H_
#include<string>
#include"CBuffer.h"
#include"CTcpConnection.h"
#include"boost/function.hpp"
class Coder
{
    public:
        using StringMessageFunc=boost::function<void(const boost::shared_ptr<TcpConnection> &,const string&)>;
        Coder();
        ~Coder(){}
        void setStringMessageCallback(const StringMessageFunc&func){m_messageCb=func;}
        void onMessage(const boost::shared_ptr<TcpConnection> &, Buffer *);
        void send(const boost::shared_ptr<TcpConnection> &,const std::string&);
        void encode(const std::string&);
        bool decode(Buffer*);
        static int kPreparesize;
    private:
        Buffer m_sendbuffer;
        string m_recievemessage;
        StringMessageFunc m_messageCb;
};












#endif