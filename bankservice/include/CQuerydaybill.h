#ifndef CQUERYDAYBILL_H_
#define CQUERYDAYBILL_H_

#include "CQueryservice.h"
#include "boost/shared_ptr.hpp"

class Querydaybill : public Queryservice
{

public:
    Querydaybill(Mysqlconn *conn, const std::string &account) : Queryservice(account, conn) {}
    virtual ~Querydaybill();
    virtual void parseMessage(const std::string &)override;
    virtual void onResponce() override;
    virtual void excuteSQL() override;

private:
    struct Transmessage
    {
         std::string accountId;
         std::string date;
         std::string money;
         std::string otherId;
         std::string balance;
    };
    boost::shared_ptr<Transmessage> m_trans;
    std::string m_date;
};

#endif