#include "CWithdraw.h"

Withdraw::Withdraw(Mysqlconn *conn, std::string user) : Accountservice(user, conn)
{
}

Withdraw::~Withdraw()
{
}

void Withdraw::parseMessage(const std::string &message)
{
    auto begin = message.find_first_of('=');
    auto end = message.find_first_of('&');
    m_amount = stof(message.substr(begin + 1, end - begin - 1));
}

void Withdraw::onResponce()
{

    char command[100] = "";
    sprintf(command, "select balance from bank_account where account_id='%s'", m_account.c_str());
    m_recordSet = m_sqlconn->querySQL(command);
    auto balance = stof(m_recordSet.getItemByPosition(0, 0));

    if (balance < m_amount)
    {
        m_targetvalue = HTMLPATH + "/withdrawError.html";
    }
    else
    {
        sprintf(command, "update bank_account set balance=balance-%.2f where account_id='%s'", m_amount, m_account.c_str());
        if(m_sqlconn->excuteSQL(command))
        {   
            m_targetvalue = HTMLPATH + "/welcome.html";
        }
    }
}