#ifndef CHATCLIENT_H_
#define CHATCLIENT_H_

#include <sys/epoll.h>
#include <stdio.h>
#include <sys/socket.h>
#include "CSocket.h"
#include <iostream>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include "CEpoll.h"
#include "Thread.h"
#include "CEventLoop.h"
#include "CCoder.h"
#include "CInetaddr.h"
#include "CEventLoopthread.h"
#include <boost/bind.hpp>
#include "CTcpClient.h"
using namespace std;

class Chatclient
{
public:
    Chatclient(int port, EventLoop *loop, const char *ip);
    void onClose();
    ~Chatclient();
    void write(const string &str);

    void connect();
    void disconnect();
    void onConnect(const boost::shared_ptr<TcpConnection> &conn);
    void onMessage(const boost::shared_ptr<TcpConnection> &conn, Buffer *buf);
    void onWriteCompelte(const boost::shared_ptr<TcpConnection> &conn);

private:
    TcpClient m_client;
    boost::shared_ptr<TcpConnection> m_conn;
    mutex m_mutex;
    Coder m_coder;
};


#endif