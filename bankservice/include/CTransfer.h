#ifndef CTRANSFER_H_
#define CTRANSFER_H_
#include "CAccountservice.h"

class Transfer : public Accountservice
{

public:
    Transfer(Mysqlconn *conn, std::string user);
    virtual ~Transfer();
    virtual void parseMessage(const std::string &) override;
    virtual void onResponce() override;

private:
    std::string m_targetUser;
};



#endif