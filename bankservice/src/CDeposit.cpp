#include "CDeposit.h"

Deposit::Deposit(Mysqlconn *conn, std::string user)
:Accountservice(user,conn)
{

}

Deposit::~Deposit()
{

}

void Deposit::parseMessage(const std::string &message)
{
    auto begin = message.find_first_of('=');
    auto end = message.find_first_of('&');
    m_amount = stof(message.substr(begin + 1, end - begin - 1));
}

void Deposit::onResponce() {
 
     char command[100] = "";
    sprintf(command, "update bank_account set balance=balance+%.2f where account_id='%s'", m_amount, m_account.c_str());
    if(m_sqlconn->excuteSQL(command))
    {
        m_targetvalue=HTMLPATH+"/welcome.html";
    }
}
