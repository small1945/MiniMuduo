#ifndef CQUERYBALANCE_H_
#define CQUERYBALANCE_H_
#include "CQueryservice.h"

class Querybalance : public Queryservice
{

public:
    Querybalance(Mysqlconn *conn,const std::string &account):Queryservice(account,conn){}
    virtual ~Querybalance();
    virtual void parseMessage(const std::string&)override;

    virtual void onResponce() override;
    virtual void excuteSQL()override;


};



#endif