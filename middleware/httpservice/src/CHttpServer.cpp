#include "CHttpServer.h"

HttpServer::HttpServer(int port, EventLoop *loop, const char *ip) : m_loop(loop),
                                                                    m_server(loop, port, ip)

{
    m_server.setLoopNum(5);
    m_server.setMessageCallback(boost::bind(&HttpServer::onMessage, this, _1, _2));
    m_server.setConnectCallback(boost::bind(&HttpServer::onConnect, this, _1));
    
}

void HttpServer::start()
{
    m_server.start();
    m_loop->loop();
}
void HttpServer::onConnect(const boost::shared_ptr<TcpConnection> &conn)
{
    if (conn->IsConnected())
    {
        cout << conn->getName() << ":connect with " << conn->getClientIP() << ":" << conn->getClientPort() << " threadID:" << conn->getThreadId() << endl;
    }
}
void HttpServer::onMessage(const boost::shared_ptr<TcpConnection> &conn, Buffer *buf)
{
    auto str = buf->retrieveStr(buf->readableBytes());
    m_httpcontext.parseContent(str.c_str());

    auto httprequest = m_httpcontext.getRequst();
    if (m_httpcontext.IsParseSuccess())
    {
        httprequest.setIsLegal(true);
    }
    else
    {
        httprequest.setIsLegal(false);
    }

    if (m_requestCb)
    {
        m_response = m_requestCb(conn,httprequest);
    }
    else
    {
        m_response = defaultRequest(httprequest);
    }

    Buffer buffer;
    m_response.addToBuffer(&buffer);

    conn->send(buffer);
}

Httpresponse HttpServer::defaultRequest(const Httprequest &request)
{
    Httpresponse retresponse;
    auto url = request.url();
    if (url == "/")
    {
        retresponse.setBody("default");
    }
    retresponse.setServername("reactor");
    return retresponse;
}
