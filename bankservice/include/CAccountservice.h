#ifndef CACCOUNTSERVICE_H_
#define CACCOUNTSERVICE_H_
#include"CBankservice.h"

class Accountservice:public Bankservice
{
    public:
        Accountservice( std::string account,Mysqlconn*conn):Bankservice(conn,account),m_amount(0){}
        virtual ~Accountservice();
        virtual void parseMessage(const std::string&)=0;
        virtual void onResponce()=0;
        void startservice(const std::string&);

    protected:
        float m_amount;

  
};





#endif