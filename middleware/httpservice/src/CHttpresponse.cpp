#include"CHttpresponse.h"

Httpresponse::Httpresponse():
            m_version(),
            m_servername(),
            m_statusStr(),
            m_statusCode(-1),
            m_body(),
            m_status(Status::NONE)
{

}

void  Httpresponse::setStatus(Status s)
{
    m_status=s;
    if(s==Status::BADREQUEST)
    {
        m_statusStr="Bad Request";
        m_statusCode=400;
    }
    else if(s==Status::ERROR)
    {}
    else if(s==Status::FORBIDDEN)
    {}
    else if(s==Status::NOTFOUND)
    {}
    else if(s==Status::OK)
    {
        
        m_statusStr="OK";
        m_statusCode=200;
    }
}


void Httpresponse::addToBuffer(Buffer *buffer)
{
    
    auto httpversion=m_version;
    auto statuscode=std::to_string(m_statusCode);
    auto status=m_statusStr;
    const string responseLine=httpversion+" "+statuscode+" "+status+"\r\n";
    buffer->append(responseLine.c_str(),responseLine.size());

     string reponseHead="Server:"+m_servername+"\r\n"+"Connection:"+m_connection+"\r\n"+"Content-Type:text/html"+"\r\n";
    
   
    if(m_status==Status::OK)
   {  

       reponseHead+="Content-Length:"+to_string(m_body.size())+"\r\n";
       buffer->append(reponseHead.c_str(),reponseHead.size());
       const string reponsebody="\r\n"+m_body;
       buffer->append(reponsebody.c_str(),reponsebody.size());
   }
   else
    {
        buffer->append(reponseHead.c_str(),reponseHead.size());
    }
}


