#include"CLogout.h"

Logout::~Logout()
{
}

void Logout::parseMessage(const std::string &message)
{
    auto begin = message.find_first_of('=');
    auto end = message.find_first_of('&');
    m_account = message.substr(begin + 1, end - begin - 1);
    begin = message.find_last_of('=');
    m_passwd = message.substr(begin + 1, message.size() - begin - 1);
}

void Logout::excuteSQL()
{
     char command[100] = "";
     sprintf(command, "select account_id from bank_account where account_id='%s'and passwd='%s'", m_account.c_str(), m_passwd.c_str());
     m_recordSet = m_sqlconn->querySQL(command);
}

void Logout::onResponce()
{
     if (m_recordSet.isEmpty())
        {
            m_targetvalue = HTMLPATH+"logoutError.html";
        }
        else
        {
             char command[100] = "";
             sprintf(command, "delete from bank_account where account_id='%s'", m_account.c_str());
             if(m_sqlconn->excuteSQL(command))
             {
                m_targetvalue= HTMLPATH+"judge.html";
             }
        }
}

