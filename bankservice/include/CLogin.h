#ifndef CLOGIN_H_
#define CLOGIN_H_

#include "CIdentityservice.h"
class Login : public Identityservice
{
public:
    Login(Mysqlconn *);
    virtual ~Login();
    virtual void parseMessage(const std::string &) override;
    virtual void excuteSQL() override;
    virtual void onResponce() override;

};

#endif