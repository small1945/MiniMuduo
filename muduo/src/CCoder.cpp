#include"CCoder.h"


int Coder::kPreparesize=4;

Coder::Coder():m_sendbuffer(),m_recievemessage()
{

}
void Coder::encode(const std::string&message)
{
    m_sendbuffer.append(message.c_str(),message.size());
    int len=message.size();
    int32_t length=static_cast<int32_t>(len);
    int32_t sendlen=htonl(length);
    m_sendbuffer.prepend(&sendlen,sizeof(sendlen));
}


void Coder::onMessage(const boost::shared_ptr<TcpConnection> &conn, Buffer *buffer)
{
    if(!decode(buffer))
    {
        conn->shutdown();
    }
    else
    {
        m_messageCb(conn,m_recievemessage);
    }
    
}

bool Coder::decode(Buffer*buffer)
{
    m_recievemessage.clear();
    while(buffer->readableBytes()>=kPreparesize)
    {
        const void*ptLen=buffer->peek();
        int32_t templen= *static_cast<const int32_t*>(ptLen);
        int32_t len=ntohl(templen);
        if(len>65536||len<0)
        {
            return false;
        }
        else if(buffer->readableBytes()>=kPreparesize+len)
        {
            buffer->retrieve(kPreparesize);    
            m_recievemessage+=buffer->retrieveStr(len);
        }
        else
        {
            break;
        }
    }
    return true;
}

void Coder::send(const boost::shared_ptr<TcpConnection> &conn,const string&message)
{
    encode(message);
    conn->send(m_sendbuffer);
}
