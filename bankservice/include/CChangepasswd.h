#ifndef CCHANGERPASSWD_H_
#define CCHANGERPASSWD_H_
#include "CIdentityservice.h"
class Changepasswd:public Identityservice 
{
    public:
        Changepasswd(Mysqlconn* conn):Identityservice(conn){}
        virtual ~Changepasswd();
        virtual void parseMessage(const std::string&);
        virtual void excuteSQL();
        virtual void onResponce();
    
    private:
        std::string m_newpasswd;
    
    

};






#endif