#include"CMysqlrecordset.h"


const std::string& Mysqlrecordset::getItem(int row,const std::string&field)
{
      
    int col=_getFieldIndex(field);
    return getItemByPosition(row,col);
}

 int Mysqlrecordset::_getFieldIndex(const std::string&target)
{
    for(unsigned int i=0;i<fields.size();i++)
    {
        if(target==fields[i])
        {
            return i;
        }
    }
    return -1;
}

int Mysqlrecordset::_getValueIndex(const std::string&value)
{
    for(unsigned int i=0;i<rows.size();i++)
    {
        for(unsigned int j=0;j<rows[0].size();j++)
        {
            if(rows[i][j]==value)
            {
                return i;
            }
        }
    }
    return -1;
}

void Mysqlrecordset::clear()
{
    rows.clear();
    fields.clear();
}


const std::string& Mysqlrecordset:: getItemByPosition(int row,int col)
{
    return rows[row][col];
}
