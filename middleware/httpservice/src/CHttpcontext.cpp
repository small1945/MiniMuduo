#include "CHttpcontext.h"
#include "boost/make_shared.hpp"
Httpcontext::Httpcontext() : m_state(State::NONE), m_request(boost::make_shared<Httprequest>()),result(),m_IsParseSuccess(false)
{
}

void Httpcontext::_parseReqBody(const char *ptbegin)
{
    if(m_request->methon()==Httprequest::Methon::GET)
    {
       m_request->setBody("");
       return;
    }
    auto ptend=ptbegin;
    while(*ptend!='\0')
    {
        ptend++;
    }
    string body(ptbegin,ptend);
    m_request->setBody(body);
    m_state=State::REQSUCESS;
}

void Httpcontext::_parseReqHead(const char *ptbegin, const char *ptend)
{
    auto& headline=m_request->getHeadline();
    if (*ptbegin == '\r' && *(ptbegin+1) == '\n'&&ptbegin==ptend)
    {
        _setState(State::REQBODY);
        return;
    }

    auto ptc = ptbegin;
    while (*ptc != ':')
    {
        ptc++;
    }
    string headlabel(ptbegin, ptc);
    ptbegin=ptc+2;
    while(*ptc!='\r')
    {
        ptc++;
    }
    string headcontent(ptbegin,ptc);
    headline[headlabel]=headcontent;
    if(headlabel=="Connection"&&headcontent=="keep-alive")
    {
        m_request->setKeepAlive();
    }
}

void Httpcontext::_parseReqline(const char *ptbegin, const char *ptend)
{
    auto ptc = ptbegin;

    while (*ptc != ' ')
    {
        ptc++;
    }
    string methon(ptbegin, ptc);
    m_request->setMethon(methon);
    ptc=_parseblank(ptc);

    ptbegin = ptc;

    while (*ptc != ' ')
    {
        ptc++;
    }
     string url(ptbegin, ptc);
    m_request->setUrl(url);
    ptc=_parseblank(ptc);
    ptbegin=ptc;

    string version(ptbegin, ptend);
    m_request->setVersion(version);
    _setState(State::REQHEAD);
}

const char* Httpcontext::_parseline(const char *ptbegin)
{
    auto ptc = ptbegin;
    while (*ptc != '\r'&&*ptc!='\0')
    {
        ptc++;
    }
    switch (m_state)
    {
    case State::REQLINE:
        _parseReqline(ptbegin, ptc);
        break;

    case State::REQHEAD:
        _parseReqHead(ptbegin, ptc);
        break;

    case State::REQBODY:
        _parseReqBody(ptbegin);
        break;
    }

    for (int i = 0; i < 2; i++)
    {
        ptc++;    
    }
    auto ptend=ptc;
    return ptend;
}

const char* Httpcontext::_parseblank( const char *ptc)
{
    while (*ptc == ' ')
    {
        ptc++;
    }
    return ptc;
 
}
void Httpcontext::_parse(const char *content)
{
    auto ptc = content;
    while (*ptc != '\0')
    {
        ptc=_parseline(ptc);
        if (m_state == State::REQBAD)
        {
            break;
        }
    }
}

void Httpcontext::parseContent(const char *buf)
{
    _setState(State::REQLINE);
    _parse(buf);
    if (m_state == State::REQBAD)
    {
        m_IsParseSuccess=false;
    }
    else{
        m_IsParseSuccess=true;
    }
}





