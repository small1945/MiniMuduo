#ifndef CBANKSERVICE_H_
#define CBANKSERVICE_H_
#include"boost/bind.hpp"
#include"boost/function.hpp"
#include"CMysqlconn.h"
#include"CMysqlrecordset.h"
#include"string.h"
const std::string HTMLPATH="../bankservice/HTML/";


class Bankservice
{
    public:
        Bankservice(Mysqlconn*conn, std::string account=""):m_sqlconn(conn),m_account(account),m_resultIsFile(true){}
        virtual ~Bankservice();
        virtual void startservice(const std::string&)=0;
        const std::string& getTarget()const{return m_targetvalue;}
        const std::string& getUser()const{return m_name;}
        const std::string& getAccount()const{return m_account;}
        bool resultIsFile()const{return m_resultIsFile;}

    protected:
        Mysqlconn* m_sqlconn;
        Mysqlrecordset m_recordSet;
        std::string m_targetvalue;
        std::string m_name;
        std::string m_account;
        bool m_resultIsFile;
};








#endif