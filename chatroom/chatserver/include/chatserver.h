#ifndef CHATSERVER_H_
#define CHATSERVER_H_

#include <sys/epoll.h>
#include <stdio.h>
#include <sys/socket.h>
#include "CSocket.h"
#include <iostream>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include "CEpoll.h"
#include "Thread.h"
#include "CEventLoop.h"
#include "CAcceptor.h"
#include "CTcpServer.h"
#include "CInetaddr.h"
#include "CEventLoopthread.h"
#include <boost/bind.hpp>
#include"CCoder.h"
using namespace std;

class Chatserver
{
public:
    using ConnectionSet=set<boost::shared_ptr<TcpConnection>> ;
    Chatserver(int port, EventLoop *loop, const char *ip) ;
    ~Chatserver() ;
    void start();
    void onConnect(const boost::shared_ptr<TcpConnection> &conn);
    void onWriteComplete(const boost::shared_ptr<TcpConnection> &conn);
    void onStringMessage(const boost::shared_ptr<TcpConnection> &conn,const string&message);
    void boardcast(const boost::shared_ptr<TcpConnection> &conn, const string&str);
    
    

private:

   boost::shared_ptr<ConnectionSet> _getConnections();

    EventLoop *m_loop;
    Tcpserver m_server;
    Coder m_coder;
    //ConnectionSet Connections;
    boost::shared_ptr<ConnectionSet> Connections;
    mutex m_mutex;
};


#endif