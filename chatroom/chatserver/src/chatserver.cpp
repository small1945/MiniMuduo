#include "chatserver.h"

Chatserver::Chatserver(int port, EventLoop *loop, const char *ip) : m_loop(loop),
                                                                    m_server(loop, port, ip),
                                                                    Connections(new ConnectionSet())

{
    m_coder.setStringMessageCallback(boost::bind(&Chatserver::onStringMessage, this, _1, _2));
    m_server.setLoopNum(10);
    m_server.setConnectCallback(boost::bind(&Chatserver::onConnect, this, _1));
    m_server.setMessageCallback(boost::bind(&Coder::onMessage, m_coder, _1, _2));
    m_server.setWriteCompleteCallback(boost::bind(&Chatserver::onWriteComplete, this, _1));
}
Chatserver::~Chatserver() {}

void Chatserver::start()
{
    m_server.start();
    m_loop->loop();
}

void Chatserver::onConnect(const boost::shared_ptr<TcpConnection> &conn)
{
    if (!Connections.unique())
    {
        Connections.reset(new ConnectionSet(*Connections));
    }
    if (conn->IsConnected())
    {
        cout << conn->getName() << ":connect with " << conn->getClientIP() << ":" << conn->getClientPort() << " threadID:" << conn->getThreadId() << endl;
        Connections->insert(conn);
    }
    else
    {
        Connections->erase(conn);
    }
}

void Chatserver::onWriteComplete(const boost::shared_ptr<TcpConnection> &conn)
{
}

void Chatserver::onStringMessage(const boost::shared_ptr<TcpConnection> &conn, const string &message)
{
    boardcast(conn, message);
}


boost::shared_ptr<Chatserver::ConnectionSet> Chatserver::_getConnections()
{
       lock_guard<mutex>lock(m_mutex);
       return Connections;
}


void Chatserver::boardcast(const boost::shared_ptr<TcpConnection> &conn, const string &str)
{
    auto bytes = str.size();
    printf("receive %d bytes from %s:%d,message is [%s]\n", bytes, conn->getClientIP().c_str(), conn->getClientPort(), str.c_str());

    auto connections = _getConnections();
    for (auto iter = connections->begin(); iter != connections->end(); iter++)
    {
        auto &activeConn = *iter;
        if (activeConn != conn)
        {
            activeConn->send(str.c_str(), bytes);
            printf("sned to %s:%d", activeConn->getClientIP().c_str(), activeConn->getClientPort());
        }
    }
}