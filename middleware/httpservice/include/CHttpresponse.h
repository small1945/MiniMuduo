#ifndef CHTTPRESPONSE_H_
#define CHTTPRESPONSE_H_
#include<string>
#include"CHttprequest.h"
#include"CBuffer.h"
#include<map>
using namespace std;

class Httpresponse
{
    public:
        enum class Status
        {
            OK,
            NOTFOUND,
            BADREQUEST,
            FORBIDDEN,
            ERROR,
            NONE
        };
        Httpresponse();
        Httpresponse(const Httpresponse&)=default;
      
        ~Httpresponse(){}
        
        void setServername(const string&name){m_servername=name;}
        void setConneciton(const string&conn){m_connection=conn;}
        void setBody(const string&body){m_body=body;}
        void setStatus(Status s);
        void setVersion(const string&version){m_version=version;}
        int  statusCode()const{return m_statusCode;}
        const string&serverName()const{return m_servername;}
        const string&status()const{return m_statusStr;}
        const string&body()const{return m_body;}
        const string&connection()const{return m_connection;}
        bool IsResponceOk()const{return m_status==Status::OK;}

        void addToBuffer(Buffer *buffer);

    
    private:
        
        string m_version;
        string m_servername;
        string m_statusStr;
        string m_connection;
        int m_statusCode;
        string m_body;
        Status m_status;
        //map<string,string>m_reshead;

};




#endif