#include "CRegister.h"

Register::Register(Mysqlconn*conn) :Identityservice(conn){}

Register::~Register()
{
}

void Register::parseMessage(const std::string &message)
{
    auto begin = message.find_first_of('=');
    auto end = message.find_first_of('&');
    m_name = message.substr(begin + 1, end - begin - 1);
    begin = message.find_last_of('=');
    m_passwd = message.substr(begin + 1, message.size() - begin - 1);
}

void Register::excuteSQL()
{
     char command[100] = "";
     sprintf(command, "select account_id from bank_account where account_id='%s'", m_name.c_str());
     m_recordSet = m_sqlconn->querySQL(command);
}

void Register::onResponce()
{
        if (!m_recordSet.isEmpty())
        {
            m_targetvalue = HTMLPATH+"registerError.html";
        }
        else
        {
            char command[100] = "";
            sprintf(command, "insert into bank_account (passwd,balance,open_date,account_id)values('%s',0,NOW(),'%s')", m_passwd.c_str(),m_name.c_str());
            m_sqlconn->excuteSQL(command);
            m_targetvalue = HTMLPATH+"judge.html";
        }
}

