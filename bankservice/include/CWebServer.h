#ifndef CWEBSERVER_H_
#define CWEBSERVER_H_
#include"CHttpServer.h"
#include<sys/stat.h>
#include"CMysqlpool.h"
#include<utility>
#include<map>
class Mysqlconn;
class Webserver
{
    public:
        Webserver(int,EventLoop*,const char*);
        ~Webserver();
        void start();
        Httpresponse onRequest(const boost::shared_ptr<TcpConnection>&conn, const Httprequest &request);

    private:

        void _handleGet(const string&);//get filename
        void _handlePost(const string&,const string&,const boost::shared_ptr<TcpConnection>&conn);
        void _setResponseBody(Httpresponse&httpresponse);
         HttpServer m_server;
        string m_filename;
        string m_bodymessage;
        int  m_fd;
        struct stat m_filestat;
        Mysqlconnpool m_sqlconnpool;
        Mysqlconn *m_sqlconn;
        map<const boost::shared_ptr<TcpConnection>, string> usermap;
        

        

};











#endif